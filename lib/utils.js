const axios = require('axios').default
const pa11y = require('pa11y')
const path = require('path')
const puppeteer = require('puppeteer')
const fs = require('fs')
const lodash = require('lodash')
const { randomUUID } = require('crypto')
const timersPromises = require('timers/promises')

const A11YRULES = ['Principle1.Guideline1_4.1_4_3_Contrast']

function getPageURL (baseURL, article, darkTheme = false) {
  const url = new URL(baseURL)
  const articleURL = new URL(encodeURIComponent(article), url)

  if (darkTheme) {
    articleURL.searchParams.set('theme', 'dark')
  }

  return articleURL.toString()
}

function filterIssues (issues) {
  const codes = [
    'Principle1.Guideline1_4.1_4_3.G18',
    'Principle1.Guideline1_4.1_4_3.G145',
    'Principle1.Guideline1_4.1_4_3.F24.BGColour',
    'Principle1.Guideline1_4.1_4_3.F24.FGColour'
  ]

  const filteredIssues = []
  lodash.forEach(issues, (issue) => {
    lodash.forEach(codes, (code) => {
      if (lodash.includes(issue.code, code)) {
        filteredIssues.push(issue)
      }
    })
  })

  return filteredIssues
}

async function retryDelayed (callable, delay = 1000) {
  try {
    return await callable()
  } catch (err) {
    console.log(`Retrying after error: ${err}`)
    await timersPromises.setTimeout(delay)
    return await callable()
  }
}

async function runPa11y (url, config) {
  const uuidTheme = randomUUID()
  const pageResults = {}
  const browser = await retryDelayed(async () => { return await puppeteer.launch() })
  const page = await retryDelayed(async () => { return await browser.newPage() })
  page.on('response', function (response) {
    pageResults.statusCode = response.status()
    pageResults.headers = response.headers()
  })
  config.browser = browser
  config.page = page
  const pa11yResults = await pa11y(url, config)
  pa11yResults.issues = filterIssues(pa11yResults.issues)
  page.evaluate((pa11yResults) => {
    pa11yResults.issues.forEach(element => {
      try {
        document.querySelector(element.selector).style.border = '2px solid red'
      } catch (err) {
        console.log(err)
      }
    })
  }, pa11yResults)

  const screenshotBasePath = path.join(__dirname, '../results/screenshots')

  if (!fs.existsSync(screenshotBasePath)) {
    fs.mkdirSync(screenshotBasePath, {
      recursive: true
    })
  }

  const screenshotPath = path.join(screenshotBasePath, `${uuidTheme}.png`)
  await retryDelayed(async () => { return await page.screenshot({ path: screenshotPath, fullPage: true }) })
  pageResults.uuidTheme = uuidTheme

  const results = { ...pageResults, ...pa11yResults }
  await retryDelayed(async () => { browser.close() })
  return results
}

async function testArticle (baseURL, article) {
  const config = {
    rules: A11YRULES
  }

  const resultsDefaultTheme = await runPa11y(getPageURL(baseURL, article), config)
  const resultsDarkTheme = await runPa11y(getPageURL(baseURL, article, true), config)

  const uniqueDefault = lodash.map(
    resultsDefaultTheme.issues,
    lodash.partialRight(lodash.pick, ['code', 'selector'])
  )

  const uniqueDark = lodash.map(
    resultsDarkTheme.issues,
    lodash.partialRight(lodash.pick, ['code', 'selector'])
  )

  const defaultThemeIssuesCount = lodash.size(resultsDefaultTheme.issues)
  const darkThemeIssuesCount = lodash.size(resultsDarkTheme.issues)

  return {
    hasDiffIssues: !lodash.isEqual(uniqueDark, uniqueDefault),
    resultsDefaultTheme,
    resultsDarkTheme,
    defaultThemeIssuesCount,
    darkThemeIssuesCount
  }
}

function datasetReader (datasetPath) {
  const data = JSON.parse(fs.readFileSync(datasetPath))
  return lodash.map(data.titles, (title) => title)
}

function datasetWriter (dataset) {
  try {
    const datasetPath = path.join(__dirname, '../dataset')
    if (!fs.existsSync(datasetPath)) {
      fs.mkdirSync(datasetPath, {
        recursive: true
      })
    }
    if (dataset) {
      fs.writeFile(`${datasetPath}/dataset.json`, JSON.stringify(dataset), err => {
        if (err) {
          console.error('Error while writing dataset ', err)
        }
      })
      console.log('Dataset written successfully')
    }
  } catch (err) {
    console.error(err)
  }
}

function resultWriter (result) {
  try {
    const datasetPath = path.join(__dirname, '../results')
    if (!fs.existsSync(datasetPath)) {
      fs.mkdirSync(datasetPath, {
        recursive: true
      })
    }
    if (result) {
      const uuidResult = randomUUID()
      fs.writeFile(`${datasetPath}/${uuidResult}.json`, JSON.stringify(result), err => {
        if (err) {
          console.error('Error while writing results ', err)
        }
      })
      console.log(`Results written succesfully: ${uuidResult}`)
    }
  } catch (err) {
    console.error(err)
  }
}

async function getTemplateCategory (url, apiParams, articlesCount) {
  const host = new URL(url).host
  let requestCount = 0
  let cmcontinue = null
  let titles = []
  articlesCount = articlesCount || Infinity

  while (articlesCount && (cmcontinue || requestCount === 0)) {
    const next = await nextFetchTemplateCategory(url, apiParams, cmcontinue)
    requestCount += 1
    cmcontinue = next.continue?.cmcontinue
    const responseTitles = lodash.map(next.query.categorymembers, 'title')
    const titlesSlice = responseTitles.slice(0, articlesCount)
    titles = lodash.concat(titles, titlesSlice)
    articlesCount -= titlesSlice.length
  }

  return { host, titles }
}

async function nextFetchTemplateCategory (url, params, cmcontinue) {
  const res = await axios({
    method: 'get',
    url,
    params: {
      ...params,
      cmlimit: 100,
      cmcontinue
    }
  })
  return res.data
}

exports.testArticle = testArticle
exports.datasetReader = datasetReader
exports.datasetWriter = datasetWriter
exports.resultWriter = resultWriter
exports.getPageURL = getPageURL
exports.getTemplateCategory = getTemplateCategory
