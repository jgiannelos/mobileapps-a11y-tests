import glob
import json
import os
import pandas as pd
import shutil


def parseResults(file):
    output = []
    results = json.load(file)
    for result in results:
        entry = {
            "hasDiffIssues": result["hasDiffIssues"],
            "default_statusCode": result["resultsDefaultTheme"]["statusCode"],
            "default_uuid": result["resultsDefaultTheme"]["uuidTheme"],
            "default_documentTitle": result["resultsDefaultTheme"]["documentTitle"],
            "default_pageUrl": result["resultsDefaultTheme"]["pageUrl"],
            "default_issueCount": result["defaultThemeIssuesCount"],
            "dark_statusCode": result["resultsDarkTheme"]["statusCode"],
            "dark_uuid": result["resultsDarkTheme"]["uuidTheme"],
            "dark_documentTitle": result["resultsDarkTheme"]["documentTitle"],
            "dark_pageUrl": result["resultsDarkTheme"]["pageUrl"],
            "dark_issueCount": result["darkThemeIssuesCount"],
        }

        output.append(entry)
    return output


def getDataframe(resultsGlobPath):
    dataset = []
    for file in glob.glob(resultsGlobPath):
        with open(file) as f:
            results = parseResults(f)
            dataset += results
    return pd.DataFrame(dataset)


def moveScreenshots(df, screenshotsBasePath):
    os.makedirs(screenshotsBasePath, exist_ok=True)
    uuids = df["default_uuid"].to_list() + df["dark_uuid"].to_list()
    for uuid in uuids:
        shutil.copy(f"../results/screenshots/{uuid}.png", screenshotsBasePath)
    return df


def getScreenshot(uuid, basepath):
    url = f"{basepath}/{uuid}.png"
    return f'<a href="{url}">{url}</a>'


def addScreenshotsUrl(df, screenshotsBasePath):
    df["default_screenshot"] = df["default_uuid"].apply(
        getScreenshot, args=[screenshotsBasePath]
    )
    df["dark_screenshot"] = df["dark_uuid"].apply(
        getScreenshot, args=[screenshotsBasePath]
    )
    return df


def processDataFrame(df):
    df["diffIssuesCount"] = df["default_issueCount"] - df["dark_issueCount"]
    df = df[df.hasDiffIssues == True].copy()
    df = moveScreenshots(df, "../report/screenshots")
    df = addScreenshotsUrl(df, "/report/screenshots")
    df.sort_values(by=["diffIssuesCount"], inplace=True)
    return df


def main():
    df = getDataframe("../results/*.json")
    df = processDataFrame(df)
    df.to_html("../report/index.html", render_links=True, escape=False)
