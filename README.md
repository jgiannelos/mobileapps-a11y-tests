# Accessibility tests for mobileapps API

Current scope is to test for dark theme rendering issues on mobileapps API but it can be expanded to more accessibility tests.

## Installation

```bash
npm install
```

## Usage

```bash
node index.js --help
```

### Steps to test data

1. Retrieve template testcase articles.

   Example of the command:
   ```bash
   node index.js buildDatasetTemplates --apiurl "https://en.wikipedia.org/w/api.php" \
                  --action "query" \
                  --list "categorymembers" \
                  --cmtitle "Category:Template_test_cases" \
                  --articlesCount 800 \
                  --format json
   ```
   If `--articlesCount` isn't set, all titles will be retrieved.

   As a result, `dataset.json` file will be written into the `/dataset` folder.
   Example of the output:

   ```json
   {
      "host": "en.wikipedia.org",
      "titles":[
         "Template:Automarkup",
         "Template:Collapsible 2 test case",
         "Template:Collapsible test case",
      ]
   }
   ```

   Example of MW API query to get subsequent list of templates testcases pages:
   https://en.wikipedia.org/w/api.php?action=query&list=categorymembers&cmtitle=Category%3ATemplate_test_cases&format=json

2. Run command that tests titles in the `dataset.json` file. Make sure that the host in dataset.json and --baseurl are the same (`en.wikipedia.org` in this example).

   Example with templates dataset
   ```bash
   node index.js test \
      --baseurl "https://en.wikipedia.org/api/rest_v1/page/mobile-html/" \
      --dataset-path "dataset/dataset.json" \
      --concurrency 1
   ```

## Resources:
- Docs about template category MW API query: https://www.mediawiki.org/wiki/API:Categorymembers
- Accessibility testing too: https://pa11y.org/
- WCAG 2.1 Standard: Summary page: https://squizlabs.github.io/HTML_CodeSniffer/Standards/WCAG2/
