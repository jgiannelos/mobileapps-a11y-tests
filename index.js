const { program } = require('commander')
const lodash = require('lodash')
const utils = require('./lib/utils')

program
  .command('test')
  .requiredOption('--baseurl <url>', 'Mobileapps API endpoint')
  .requiredOption('--dataset-path <path>', 'Articles to be checked')
  .option('--concurrency <integer>', 'Number of concurrent tests', parseInt, 0)
  .action(async (opts) => {
    const titles = utils.datasetReader(opts.datasetPath)
    const chunks = lodash.chunk(titles, opts.concurrency)

    for (const chunk of chunks) {
      const partial = lodash.partial(utils.testArticle, opts.baseurl)
      const tests = lodash.map(chunk, partial)
      const results = await Promise.all(tests).catch(err => console.log('Error while fetching data: ', err))
      utils.resultWriter(results)
    }
  })

program
  .command('buildDatasetTemplates')
  .requiredOption('--apiurl <url>', 'Mediawiki API endpoint')
  .option('--action <string>', 'Type of action', 'query')
  .option('--list <string>', 'Type of list. Default: categorymembers', 'categorymembers')
  .requiredOption('--cmtitle <string>', 'Which category to enumerate')
  .option('--articlesCount <number>', 'How many articles to retrieve', lodash.partialRight(parseInt, 10))
  .option('--format <string>', 'Format of the retrieving data', 'json')
  .action(async (opts) => {
    const { apiurl, action, list, cmtitle, articlesCount, format } = opts
    const apiParams = { action, list, cmtitle, format }
    const dataset = await utils.getTemplateCategory(apiurl, apiParams, articlesCount)
    utils.datasetWriter(dataset)
  })

program.parse()
